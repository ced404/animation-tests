(function() {
	"use strict";

	// Animations
	let animations = {
		slideIn: {scale: 0.7, ease: Expo.easeOut}
	};

	// Elements
	let slidersWrapper = document.querySelector('[animate-all="slide-in"]');
	let sliders = document.querySelectorAll(
		'[animate-all="slide-in"] > *, [animate="slide-in"]'
	);

	const animationCompleted = el => {
		el.classList.add("animation-completed");
	};

	const slideIn = () => {
		let delay = 0;
		sliders.forEach(el => {
			requestAnimationFrame(() => {
				// TweenMax.to(el, 1, animations.slideIn);
				TweenLite.to(el, 0.6, {
					x: 0,
					y: 0,
					ease: Expo.easeOutQuad,
					delay: delay
				});
				TweenLite.to(el, 0.8, {
					opacity: 1,
					ease: Expo.easeOutQuad,
					delay: delay
				});
				delay += 0.2;
			});
		});
	};

	slideIn();
})();
